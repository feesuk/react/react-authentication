import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navigation from "./Navigation";
import Home from "./Pages/Home";
import Login from "./Pages/Login";
import Logout from "./Pages/Logout";

function App() {
  return (
    <Router>
      <div>
        <h1>Login App</h1>
        <hr />
        <Navigation />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/login" component={() => <Login />} />
          <Route path="/logout">
            <Logout />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
