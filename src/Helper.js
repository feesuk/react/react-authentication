import Cookies from "js-cookie";

export function checkLogin() {
  if (typeof Cookies.get("token") === "undefined") {
    console.log("Belum Login");
    return false;
  } else {
    console.log("Sudah Login");
    return true;
  }
}
