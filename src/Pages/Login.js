import React from "react";
import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";

export default function Login(props) {
  let history = useHistory();

  const handleLogin = () => {
    //pura-pura set token pake JWT
    Cookies.set("token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9");
    Cookies.set("username", "seseorang");
    Cookies.set("roles", "user");
    // redirect to home
    history.push("/");
  };

  return (
    <>
      <h3>Login</h3>
      <form onSubmit={handleLogin}>
        Username:
        <br />
        <input type="text" name="username" />
        <br />
        <br />
        Password:
        <br />
        <input type="password" name="username" />
        <br />
        <br />
        <button type="submit">Login!</button>
      </form>
    </>
  );
}
