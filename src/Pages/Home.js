import { checkLogin } from "../Helper";

export default function Home() {
  return <div>{checkLogin() ? "Sudah Login" : "Belum Login"}</div>;
}
